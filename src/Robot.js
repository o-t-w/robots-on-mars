import React, { useState} from 'react';
import './App.css';

function Robot() {
	const [rectangleWidth, setRectangleWidth] = useState(0);
	const [rectangleHeight, setRectangleHeight] = useState(0);
	const [robotPosition, setRobotPosition] = useState("")
	const [verticalPosition, setVerticalPosition] = useState(0);
	const [horizontalPosition, setHorizontalPosition] = useState(0);
	const [robotIsLost, setRobotIsLost] = useState(false);
	const [instructions, setInstructions] = useState("");
	const [output, setOutput] = useState("");
	const [allInputsValid, setAllInputsValid] = useState(false);

	function handleInstructions(value) {
		if (value.length <= 100) {
			var stringArray = value.toUpperCase().split("");
			const filteredStringArray = stringArray.filter(letter => ["L", "R", "F"].includes(letter)).join("");
			setInstructions(filteredStringArray);
			console.log(instructions);
		}
	}

	function handlePositionChange(value) {
		if (value.length <= 7) {
			let valueAsArray = [...value.toUpperCase()];
			const regex = /[0-9NESW\s]/
			let filteredValue = valueAsArray.filter(letter => regex.test(letter)).join("");
			setRobotPosition(filteredValue);
		}
	}

	function handleWidthChange(value) {
		if ((value <= 50 && value >= 0)) {
			setRectangleWidth(value);
		}
	}

	function handleHeightChange(value) {
		if (value <= 50 && value >= 0) {
			setRectangleHeight(value);
		}
	}

	function turnRobot(leftOrRight, robotOrientation) {

		if (leftOrRight === "right") {
			if (robotOrientation === "N") {
				return "E";
			}
			else if (robotOrientation === "E") {
				return "S";
			}
			else if (robotOrientation === "S") {
				return "W";
			}
			else {
				return "N";
			}
		}
		else if (leftOrRight === "left") {
			if (robotOrientation === "N") {
				return "W";
			}
			else if (robotOrientation === "W") {
				return "S";
			}
			else if (robotOrientation === "S") {
				return "E";
			}
			else {
				return "N";
			}
		}
	}

	function moveRobot(robotOrientation, verticalPosition, horizontalPosition) {
		if (robotOrientation === "N") {
			return [horizontalPosition, verticalPosition + 1];
		}
		else if (robotOrientation === "S") {
			return [horizontalPosition, verticalPosition - 1];
		}
		else if (robotOrientation === "E") {
			return [horizontalPosition + 1, verticalPosition];
		}
		else {
			return [horizontalPosition - 1, verticalPosition];
		}
	}

	function handleSubmit(event) {
		event.preventDefault();
		let [hPosition, vPosition, orientation] = robotPosition.split(" ");

		hPosition = parseInt(hPosition);
		vPosition = parseInt(vPosition);
	
		instructions.split("").forEach(instruction => {
			if (instruction === "F") {
				[hPosition, vPosition] = moveRobot(orientation, vPosition, hPosition);
			}
			else if (instruction === "R") {
				orientation = turnRobot("right");
			}
			else if (instruction === "L") {
				orientation = turnRobot("left");
			}
		});

		setHorizontalPosition(hPosition);
		setVerticalPosition(vPosition);

		setOutput(`${hPosition} ${vPosition} ${orientation}`);
		if ((vPosition > rectangleHeight) || (vPosition < 0) || (hPosition > rectangleWidth) || (hPosition < 0)) {
			setRobotIsLost(true);
		}
	}

    return (
      <div className="App">
		<form onSubmit={(event)=>handleSubmit(event)}>

		<label htmlFor="width">width of Mars:</label>
		<input className="width-2ch" onChange={(event) => handleWidthChange(event.target.valueAsNumber)} value={rectangleWidth} id="width" type="number"/>

		<label htmlFor="height">height of Mars:</label>
		<input className="width-2ch" onChange={(event) => handleHeightChange(event.target.valueAsNumber)} value={rectangleHeight} id="height" type="number"/>

		<label htmlFor="position">Robot Position:</label>
		<input className="width-10ch" onChange={(event) => handlePositionChange(event.target.value)} value={robotPosition} id="position" maxLength="100" type="text"/>

		<label htmlFor="instructions">Robot Instructions:</label>
		<input value={instructions} onChange={(event) => handleInstructions(event.target.value)} id="instructions" type="text"/>

		<button>Submit robot instructions</button>

		</form>

		<div className="mt20 label">output:</div>
		<output>{output} {robotIsLost ? "LOST" : ""}</output>
      </div>
    );
}

export default Robot;
