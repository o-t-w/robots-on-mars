import React, { Component } from 'react';
import Robot from './Robot';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
	<Robot/>
      </div>
    );
  }
}

export default App;
